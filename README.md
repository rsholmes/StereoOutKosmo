# Stereo output / Kosmo

Stereo output synthesizer module in Kosmo format.

This is somewhat based on the [Barton Stereo Outs](https://www.bartonmusicalcircuits.com/stereoouts/index.html)  module. There are four AC coupled inputs with pan and level controls. 

The headphone output section is based on the LM4808 datasheet circuit. This is an SMD (SOIC-8) part; all other components are through hole.

I've built this but my mechanical design was frankly stupid. I ended up kludging it together in a somewhat horrible way. I have eliminated the PCB design file and Gerbers from this repository; do better ones yourself! The circuit design is fine as far as I can see. So is the front panel except the slider mounting screws holes would be eliminated if I were to do it over.

## Current draw
11 mA +12 V, 6 mA -12 V

## Photo

![](Images/output_photo.jpg)

## Documentation:

* [Schematic](Docs/ao_output.pdf)
* [BOM](Docs/ao_output_bom.md)


